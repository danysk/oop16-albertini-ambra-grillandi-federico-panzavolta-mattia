##README##
This README document whatever steps are necessary to get your application up and running.

##What is this repository for?##
It's for developing an application to manage a cinema, the application�s name is �Multisala mag� and it�s a project for the course of Object-Oriented Programming, faculty of Engineering and Computer Science, University of Bologna, Italy.

##How do I get set up?##
Download jar file from 'Downloads' and just run it!

##Who do I talk to?##
Development team: Albertini Ambra, Grillandi Federico, Panzavolta Mattia
